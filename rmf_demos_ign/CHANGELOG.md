## Changelog for package rmf_demos_ign

1.3.0 (2021-09-08)
------------------
* Provides launch files for running various demonstrations in ignition-gazebo

